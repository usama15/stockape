import React from "react";
import { Link } from "react-router-dom";

class Contact extends React.Component {
  render() {
    return (
      <div>
        <div class="main-wrapper">

          <div class="page-wrapper">
          <nav class="navbar">
              <a href="#" class="sidebar-toggler">
                <i data-feather="menu"/>
              </a>
              <div class="navbar-content">
                <form class="search-form">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <i data-feather="search"/>
                      </div>
                    </div>
                    <input
                      type="text"
                      class="form-control"
                      id="navbarForm"
                      placeholder="Search here..."
                    />
                  </div>
                </form>
                <ul class="navbar-nav">
                  <li class="nav-item dropdown">
                    <a
                      class="nav-link dropdown-toggle"
                      href="#"
                      id="languageDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <i class="flag-icon flag-icon-us mt-1" title="us"/>
                      <span class="font-weight-medium ml-1 mr-1">English</span>
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
             <div class="page-content">
              <div class="row">
                <div class="col-lg-6 grid-margin">
                  <div class="card" id="myalign">
                    <div class="card-body">
                      <h4 class="card-title">Contact Us</h4>
                      <p class="card-description">
                        Read the{" "}
                        <a href="https://jqueryvalidation.org/" target="_blank">
                          {" "}
                          Official jQuery Validation Documentation{" "}
                        </a>
                        for a full list of instructions and other options.
                      </p>
                      <form
                        class="cmxform"
                        id="signupForm"
                        method="get"
                        action="#"
                      >
                        <fieldset>
                          <div class="form-group">
                            <label for="name">Name</label>
                            <input
                              id="name"
                              class="form-control"
                              name="name"
                              type="text"
                            />
                          </div>
                          <div class="form-group">
                            <label for="email">Email</label>
                            <input
                              id="email"
                              class="form-control"
                              name="email"
                              type="email"
                            />
                          </div>
                          <div class="form-group">
                            <label for="password">Password</label>
                            <input
                              id="password"
                              class="form-control"
                              name="password"
                              type="password"
                            />
                          </div>
                          <div class="form-group">
                            <label for="confirm_password">
                              Confirm password
                            </label>
                            <input
                              id="confirm_password"
                              class="form-control"
                              name="confirm_password"
                              type="password"
                            />
                          </div>
                          <input
                            class="btn btn-primary"
                            type="submit"
                            value="Submit"
                          />
                        </fieldset>
                      </form>
                    </div>
                  </div>
                </div>
              </div>

              <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
              <p class="text-muted text-center text-md-left">
                Use is subject to full acceptance of Terms of Use and Privacy
                Policy, bar none. Investing is risky and can result in huge
                losses.
                <br /> Information provided is not financial advice and we do
                not warrant or guarantee the contents are accurate, up-to-date
                or reliable.{" "}
              </p>
              <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">
                StockApe ©{" "}
                <a href="https://twitter.com/stock_ape">
                  <img src="twitter.svg" style={{ height: "20px" }} />
                </a>{" "}
                <a href="https://www.reddit.com/r/StockApeAI/">
                  <img
                    src="reddit1.svg"
                    id="redditbg"
                    style={{ height: "20px" }}
                  />
                </a>{" "}
              </p>
            </footer>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default Contact;
