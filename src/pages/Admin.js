import React from "react";
import Graph from "./Graph";
import LineBar from "./Graph/Line";
import BarChart from "./Graph/Bar";
import './Style.css'

const Admin = () => {
    return(
        <div>
            <div className="main-wrapper">

                <div className="page-wrapper">
                    <nav className="navbar">
                        <a href="#" className="sidebar-toggler">
                            <i data-feather="menu"/>
                        </a>
                        <div className="navbar-content">
                            <form className="search-form">
                                <div className="input-group">
                                    <div className="input-group-prepend">
                                        <div className="input-group-text">
                                            <i data-feather="search"/>
                                        </div>
                                    </div>
                                    <input
                                        type="text"
                                        className="form-control"
                                        id="navbarForm"
                                        placeholder="Search here..."
                                    />
                                </div>
                            </form>
                            <ul className="navbar-nav">
                                <li className="nav-item dropdown">
                                    <a
                                        className="nav-link dropdown-toggle"
                                        href="#"
                                        id="languageDropdown"
                                        role="button"
                                        data-toggle="dropdown"
                                        aria-haspopup="true"
                                        aria-expanded="false"
                                    >
                                        <i className="flag-icon flag-icon-us mt-1" title="us"/>
                                        <span className="font-weight-medium ml-1 mr-1">English</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </nav>
                    <div className="page-content">
                        <div className="row">
                            <div className="col-lg-6 grid-margin">
                                <div className="card" id="myalign">
                                    <div className="card-body">
                                        <h4 className="card-title">Form</h4>

                                        <form
                                            className="cmxform"
                                            id="signupForm"
                                            method="get"
                                            action="#"
                                        >
                                            <fieldset>
                                                <div className="form-group">
                                                    <label htmlFor="name">Name</label>
                                                    <input
                                                        id="name"
                                                        className="form-control"
                                                        name="name"
                                                        type="text"
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="email">Email</label>
                                                    <input
                                                        id="email"
                                                        className="form-control"
                                                        name="email"
                                                        type="email"
                                                    />
                                                </div>
                                                <div className="form-group">
                                                    <label htmlFor="password">Password</label>
                                                    <input
                                                        id="password"
                                                        className="form-control"
                                                        name="password"
                                                        type="password"
                                                    />
                                                </div>
                                                {/*<div className="form-group">*/}
                                                {/*    <label htmlFor="confirm_password">*/}
                                                {/*        Confirm password*/}
                                                {/*    </label>*/}
                                                {/*    <input*/}
                                                {/*        id="confirm_password"*/}
                                                {/*        className="form-control"*/}
                                                {/*        name="confirm_password"*/}
                                                {/*        type="password"*/}
                                                {/*    />*/}
                                                {/*</div>*/}
                                                <input
                                                    className="btn btn-primary"
                                                    type="submit"
                                                    value="Submit"
                                                />
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div>
                            <div className="">
                                <div className="row">
                                    <div className="col-xl-3 grid-margin stretch-card">
                                        <div className="card">
                                            <div className="card-body">
                                                <h6 className="card-title " style={{textAlign: "left"}}>
                                                    Projects
                                                </h6>
                                                <div className="table-responsive">
                                                    <table id="dataTableExample" className="table">
                                                        <thead>
                                                        <tr>
                                                            <th>
                                                                <p id="myfont">Ticker</p>
                                                            </th>
                                                            <th>
                                                                <p
                                                                    id="myfont"
                                                                    style={{color: "yellow", width: "20px"}}
                                                                >
                                                                    Hourly
                                                                </p>
                                                            </th>
                                                            <th>
                                                                <p
                                                                    id="myfont"
                                                                    style={{color: "yellow", width: "15px"}}
                                                                >
                                                                    Daily
                                                                </p>
                                                            </th>
                                                        </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        {/*<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>
                                   */}
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"/>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"/>
                                                                        </svg>
                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"/>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"/>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"/>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"/>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"></line>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"></polyline>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"/>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"/>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        <tr>
                                                            <td>
                                                                <span className="badge badge-danger">Released</span>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-danger">
                                                                        <span>-2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"/>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"/>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                            <td>
                                                                <div className="d-flex align-items-baseline">
                                                                    <p className="text-success">
                                                                        <span>2.8%</span>
                                                                        <svg xmlns="http://www.w3.org/2000/svg"
                                                                             width="24" height="24" viewBox="0 0 24 24"
                                                                             fill="none" stroke="currentColor"
                                                                             stroke-width="2" stroke-linecap="round"
                                                                             stroke-linejoin="round"
                                                                             className="feather feather-arrow-down icon-sm mb-1">
                                                                            <line x1="12" y1="5" x2="12" y2="19"/>
                                                                            <polyline
                                                                                points="19 12 12 19 5 12"/>
                                                                        </svg>

                                                                    </p>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div className="col-sm-9 ">
                                        <div className="card overflow-hidden" id="myheight">
                                            <div className="card-body">




                                                <div className="flot-wrapper "  >
                                                    <LineBar/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <br/>
                                                    <BarChart/>
                                                </div>

                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <footer
                            className="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
                            <p className="text-muted text-center text-md-left">
                                Use is subject to full acceptance of Terms of Use and Privacy
                                Policy, bar none. Investing is risky and can result in huge
                                losses.
                                <br/> Information provided is not financial advice and we do
                                not warrant or guarantee the contents are accurate, up-to-date
                                or reliable.{" "}
                            </p>
                            <p className="text-muted text-center text-md-left mb-0 d-none d-md-block">
                                StockApe ©{" "}
                                <a href="https://twitter.com/stock_ape">
                                    <img src="twitter.svg" style={{height: "20px"}}/>
                                </a>{" "}
                                <a href="https://www.reddit.com/r/StockApeAI/">
                                    <img
                                        src="reddit1.svg"
                                        id="redditbg"
                                        style={{height: "20px"}}
                                    />
                                </a>{" "}
                            </p>
                        </footer>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Admin;
