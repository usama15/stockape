import React, { Component } from "react";
import Chart from "react-apexcharts";

class BarChart extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          type: "bar",
          // height: "320",
          parentHeightOffset: 0,
        },
        colors: ["#f77eb9"],
        grid: {
          borderColor: "rgba(77, 138, 240, .1)",
          padding: {
            bottom: -15,
          },
        },
        xaxis: {
          type: "datetime",
          categories: [
            "01/01/1991",
            "01/01/1992",
            "01/01/1993",
            "01/01/1994",
            "01/01/1995",
            "01/01/1996",
            "01/01/1997",
            "01/01/1998",
            "01/01/1999",
          ],
        },
      },
      series: [
        {
          name: "sales",
          data: [30, 40, 45, 50, 49, 60, 70, 91, 125],
        },
      ],
    };
  }

  render() {
    return (
      
            <Chart
              options={this.state.options}
              series={this.state.series}
              type="bar"
              width="100%"
              height="300"
            />
    );
  }
}

export default BarChart;
