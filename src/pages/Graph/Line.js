import React, { Component } from "react";
import Chart from "react-apexcharts";

class LineBar extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          // height: 300,
          type: "line",
          parentHeightOffset: 0,
        },
        colors: ["#727cf5", "#7ee5e5", "#4d8af0"],
        grid: {
          borderColor: "rgba(77, 138, 240, .1)",
          padding: {
            bottom: -15,
          },
        },
        xaxis: {
          type: "datetime",
          categories: ["2015", "2016", "2017", "2018"],
        },
        markers: {
          size: 0,
        },
        stroke: {
          width: 3,
          curve: "smooth",
          lineCap: "round",
        },
        legend: {
          show: true,
          position: "top",
          horizontalAlign: "left",
          containerMargin: {
            top: 30,
          },
        },
        responsive: [
          {
            breakpoint: 500,
            options: {
              legend: {
                fontSize: "11px",
              },
            },
          },
        ],
      },
      series: [
        {
          name: "Data a",
          data: [45, 52, 38, 45],
        },
        {
          name: "Data b",
          data: [12, 42, 68, 33],
        },
        {
          name: "Data c",
          data: [8, 32, 48, 53],
        },
      ],
    };
  }

  render() {
    return (
      
            <Chart
              options={this.state.options}
              series={this.state.series}
              width="100%"
              height="300"
              // style={{color:'#E91E63' }}
            />
    );
  }
}

export default LineBar;
