import React from "react";
import { Link } from "react-router-dom";

class NavBar extends React.Component {
  render() {
    return (
      <nav class="sidebar">
        <div class="sidebar-header">
          <a href="#" class="sidebar-brand">
            <img src="stockape_logo.png" width="130px" />
          </a>
          <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
          </div>
        </div>
        <div class="sidebar-body">
          <ul class="nav">
            <li class="nav-item nav-category" style={{ textAlign: "left" }}>
              Main
            </li>
            <Link to="/">
              <li class="nav-item">
                <a href="" class="nav-link">
                  {/* <i class="link-icon" data-feather="box"></i> */}
                  <svg
                    style={{ color: "white" }}
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-box link-icon"
                  >
                    <path d="M21 16V8a2 2 0 0 0-1-1.73l-7-4a2 2 0 0 0-2 0l-7 4A2 2 0 0 0 3 8v8a2 2 0 0 0 1 1.73l7 4a2 2 0 0 0 2 0l7-4A2 2 0 0 0 21 16z" />
                    <polyline points="3.27 6.96 12 12.01 20.73 6.96" />
                    <line x1="12" y1="22.08" x2="12" y2="12" />
                  </svg>
                  <span class="link-title">Dashboard</span>
                </a>
              </li>
            </Link>
            <li class="nav-item nav-category" style={{ textAlign: "left" }}>
              Featured
            </li>
            <Link to="/chart">
              <li class="nav-item">
                <a href="" class="nav-link">
                  {/* <i class="link-icon" data-feather="pie-chart"></i> */}
                  <svg
                    style={{ color: "white" }}
                    className="link-icon"
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-pie-chart link-icon"
                  >
                    <path d="M21.21 15.89A10 10 0 1 1 8 2.83" />
                    <path d="M22 12A10 10 0 0 0 12 2v10z" />
                  </svg>
                  <span class="link-title">Chart</span>
                </a>
              </li>
            </Link>
            <Link to="/contact">
              <li class="nav-item">
                <a href="" class="nav-link">
                  {/* <i class="link-icon" data-feather="message-square"></i> */}
                  <svg
                    style={{ color: "white" }}
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-message-square link-icon"
                  >
                    <path d="M21 15a2 2 0 0 1-2 2H7l-4 4V5a2 2 0 0 1 2-2h14a2 2 0 0 1 2 2z" />
                  </svg>
                  <span class="link-title">Contact</span>
                </a>
              </li>
            </Link>
            <Link to="/details">
              <li class="nav-item">
                <a href="" class="nav-link">
                  {/* <i class="link-icon" data-feather="layout"></i> */}
                  <svg
                    style={{ color: "white" }}
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-layout link-icon"
                  >
                    <rect x="3" y="3" width="18" height="18" rx="5" ry="5" />
                    <line x1="3" y1="9" x2="21" y2="9" />
                    <line x1="9" y1="21" x2="9" y2="9" />
                  </svg>
                  <span class="link-title">Details</span>
                </a>
              </li>
            </Link>
            <Link to="/privacy">
              <li class="nav-item">
                <a href="" class="nav-link">
                  {/* <i class="link-icon" data-feather="unlock"></i> */}
                  <svg
                    style={{ color: "white" }}
                    xmlns="http://www.w3.org/2000/svg"
                    width="16"
                    height="16"
                    viewBox="0 0 24 24"
                    fill="none"
                    stroke="currentColor"
                    stroke-width="3"
                    stroke-linecap="round"
                    stroke-linejoin="round"
                    className="feather feather-unlock link-icon"
                  >
                    <rect x="3" y="11" width="18" height="11" rx="2" ry="2" />
                    <path d="M7 11V7a5 5 0 0 1 9.9-1" />
                  </svg>
                  <span class="link-title">Privacy</span>
                </a>
              </li>
            </Link>
            <Link to="/login">

              <li className="nav-item">
                <a href="" className="nav-link">
                  {/* <i class="link-icon" data-feather="unlock"></i> */}

                  <span className="link-title">Login</span>
                </a>
              </li>
            </Link>
          </ul>
        </div>
      </nav>
    );
  }
}

export default NavBar;
