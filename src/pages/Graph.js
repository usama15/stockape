import React, { Component } from "react";
import Chart from "react-apexcharts";

class Graph extends Component {
  constructor(props) {
    super(props);

    this.state = {
      options: {
        chart: {
          id: "bar",
          type: "line",
          background:
            "linear-gradient(180deg, rgba(0, 255, 76,0.3) 10%, rgba(255, 0, 0,0.3) 100%)",
        },
        colors: ["#727cf5"],
        xaxis: {
          show: true,
          categories: [
            "1 Jan",
            "   2 Jan",
            "3 Jan",
            "4 Jan",
            "5 Jan",
            "6 Jan",
            "7 Jan",
            "8 Jan",
          ],
          labels: {
            style: {
              colors: "#f5f2f3",
            },
          },
          style: {
            color: "#f1ecee",
          },
        },
        yaxis: [
          {
            categories: [
              "1 Jan",
              "   2 Jan",
              "3 Jan",
              "4 Jan",
              "5 Jan",
              "6 Jan",
              "7 Jan",
              "8 Jan",
            ],
            axisTicks: {
              show: true,
            },
            axisBorder: {
              show: true,
              color: "#f8f7f8",
            },
            labels: {
              style: {
                colors: "#f5f2f3",
              },
            },
            title: {
              // text: "Series A",
              style: {
                color: "#f1ecee",
              },
            },
          },
          // {
          //   opposite: false,
          //   axisTicks: {
          //     show: false
          //   },
          //   axisBorder: {
          //     show: false,
          //     color: "#247BA0"
          //   },
          //   labels: {
          //     style: {
          //       colors: "#247BA0"
          //     }
          //   },
          //   title: {
          //     text: "Series B",
          //     style: {
          //       color: "#247BA0"
          //     }
          //   }
          // }
        ],

        grid: {
          borderColor: "rgba(77, 138, 240, .1)",
        },
        stroke: {
          curve: "smooth",
        },
      },
      series: [
        {
          name: "series",
          data: [
            -1,
            -0.12,
            +0.12,
            0.25,
            0.64,
            0.95,
            0.96,
            0.97,
            0.98,
            0.99,
            0.992,
          ],
          dataLabels: {
            enabled: false,
          },
        },
      ],
    };
  }

  render() {
    return (
      <div className="app">
        <div className="row">
          <div className="mixed-chart">
            <Chart
              options={this.state.options}
              series={this.state.series}
              width="400%"
              height="400"
              // style={{color:'#E91E63' }}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Graph;
