import React, { useState, useEffect } from "react";
import Graph from "./Graph";
import Donut from "./Graph/Donut";

class Home extends React.Component {
  render() {
    return (
      <div>
        <div class="main-wrapper">
          <div class="page-wrapper">
            <nav class="navbar">
              <a href="#" class="sidebar-toggler">
                <i data-feather="menu"></i>
              </a>
              <div class="navbar-content">
                <form class="search-form">
                  <div class="input-group">
                    <div class="input-group-prepend">
                      <div class="input-group-text">
                        <i data-feather="search"></i>
                      </div>
                    </div>
                    <input
                      type="text"
                      class="form-control"
                      id="navbarForm"
                      placeholder="Search here..."
                    />
                  </div>
                </form>
                <ul class="navbar-nav">
                  <li class="nav-item dropdown">
                    <a
                      class="nav-link dropdown-toggle"
                      href="#"
                      id="languageDropdown"
                      role="button"
                      data-toggle="dropdown"
                      aria-haspopup="true"
                      aria-expanded="false"
                    >
                      <i class="flag-icon flag-icon-us mt-1" title="us"></i>{" "}
                      <span class="font-weight-medium ml-1 mr-1">English</span>
                    </a>
                  </li>
                </ul>
              </div>
            </nav>
            <div class="page-content">
              <div class="d-flex justify-content-between align-items-center flex-wrap grid-margin">
                <div>
                  <h4 class="mt-0 mb-md-0">Welcome to Dashboard</h4>
                </div>
              </div>

              <div class="row">
                <div class="col-xl-3 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h6 class="card-title " style={{ textAlign: "left" }}>
                        Projects
                      </h6>
                      <div class="table-responsive">
                        <table id="dataTableExample" class="table">
                          <thead>
                            <tr>
                              <th>
                                <p id="myfont">Ticker</p>
                              </th>
                              <th>
                                <p
                                  id="myfont"
                                  style={{ color: "yellow", width: "20px" }}
                                >
                                  Hourly
                                </p>
                              </th>
                              <th>
                                <p
                                  id="myfont"
                                  style={{ color: "yellow", width: "15px" }}
                                >
                                  Daily
                                </p>
                              </th>
                            </tr>
                          </thead>
                          <tbody>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                    {/*<svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>
                                   */}
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>
                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                            <tr>
                              <td>
                                <span class="badge badge-danger">Released</span>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-danger">
                                    <span>-2.8%</span>
                                   <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                              <td>
                                <div class="d-flex align-items-baseline">
                                  <p class="text-success">
                                    <span>2.8%</span>
                                    <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="none" stroke="currentColor" stroke-width="2" stroke-linecap="round" stroke-linejoin="round" class="feather feather-arrow-down icon-sm mb-1"><line x1="12" y1="5" x2="12" y2="19"></line><polyline points="19 12 12 19 5 12"></polyline></svg>

                                  </p>
                                </div>
                              </td>
                            </tr>
                          </tbody>
                        </table>
                      </div>
                    </div>
                  </div>
                </div>
                <div class="col-xl-9 ">
                  <div class="card overflow-hidden" id="myheight">
                    <div class="card-body">
                      <div class="d-flex justify-content-between align-items-baseline mb-4 mb-md-3">
                        <h6 class="card-title mb-0">Live Sentiment Analysis</h6>
                        <div class="dropdown">
                          <button
                            class="btn p-0"
                            type="button"
                            id="dropdownMenuButton3"
                            data-toggle="dropdown"
                            aria-haspopup="true"
                            aria-expanded="false"
                          >
                            <i
                              class="icon-lg text-muted pb-3px"
                              data-feather="more-horizontal"
                            />
                          </button>
                          <div
                            class="dropdown-menu dropdown-menu-right"
                            aria-labelledby="dropdownMenuButton3"
                          >
                            <a
                              class="dropdown-item d-flex align-items-center"
                              href="#"
                            >
                              <i data-feather="eye" class="icon-sm mr-2" />{" "}
                              <span class="">View</span>
                            </a>
                            <a
                              class="dropdown-item d-flex align-items-center"
                              href="#"
                            >
                              <i data-feather="edit-2" class="icon-sm mr-2" />{" "}
                              <span class="">Edit</span>
                            </a>
                            <a
                              class="dropdown-item d-flex align-items-center"
                              href="#"
                            >
                              <i data-feather="trash" class="icon-sm mr-2" />{" "}
                              <span class="">Delete</span>
                            </a>
                            <a
                              class="dropdown-item d-flex align-items-center"
                              href="#"
                            >
                              <i data-feather="printer" class="icon-sm mr-2" />{" "}
                              <span class="">Print</span>
                            </a>
                            <a
                              class="dropdown-item d-flex align-items-center"
                              href="#"
                            >
                              <i data-feather="download" class="icon-sm mr-2" />{" "}
                              <span class="">Download</span>
                            </a>
                          </div>
                        </div>
                      </div>
                      <div class="row align-items-start mb-2">
                        <div class="col-md-7">
                          <p
                            class="text-muted tx-13 mb-3 mb-md-0"
                            style={{ textAlign: "left" }}
                          >
                            StockApe uses deep learning analysis on multiple
                            live data points to generate a trending aggregate
                            sentiment score. Graph will auto update every
                            interval.
                          </p>
                        </div>
                        <div class="col-md-5 d-flex justify-content-md-end">
                          <div
                            class="btn-group mb-3 mb-md-0"
                            role="group"
                            aria-label="Basic example"
                          >
                            <button
                              type="button"
                              class="btn btn-outline-primary"
                            >
                              Today
                            </button>
                            <button
                              type="button"
                              class="btn btn-outline-primary d-none d-md-block active"
                            >
                              Week
                            </button>
                            <button
                              type="button"
                              class="btn btn-outline-primary"
                            >
                              Month
                            </button>
                            <button
                              type="button"
                              class="btn btn-outline-primary"
                            >
                              Year
                            </button>
                          </div>
                        </div>
                        <div class="col-md-12 d-flex justify-content-md-end">
                          <div
                            class="btn-group mb-3 mb-md-0"
                            role="group"
                            aria-label="Basic example"
                          >
                            <img src="1.png" width="30px" height="40px" />
                            <img src="2.png" width="30px" height="40px" />
                            <img src="3.png" width="30px" height="40px" />
                            <img src="4.png" width="30px" height="40px" />
                            <img src="5.png" width="30px" height="40px" />
                            <img src="6.png" width="30px" height="40px" />
                            <img src="7.png" width="30px" height="40px" />
                            <img src="8.png" width="30px" height="40px" />
                            <img src="9.png" width="30px" height="40px" />
                            <img src="10.png" width="30px" height="40px" />
                          </div>
                        </div>
                      </div>

                      <div class="row">
                        <h6
                          id="newfont"
                          style={{ color: "green", marginBottom: "10px" }}
                        >
                          Bullish
                        </h6>
                      </div>

                      <div className="flot-wrapper" style={{ height: "400px" }}>

                        <Graph className="flot-chart mygraph" />
                      </div>
                      <div className="row">
                        <h6
                          id="newfont"
                          style={{ color: "red", marginLeft: "" }}
                        >
                          Bearish
                        </h6>
                      </div>
                    </div>
                  </div>
                </div>
              </div>

              <div class="row"/>
              <br />
              <div class="row">
                <div class="col-xl-6 grid-margin stretch-card">
                  <div class="card">
                    <div class="card-body">
                      <h6 class="card-title">MOST ACTIVE</h6>
                      {/* <div id="apexDonut" /> */}
                      <Donut />
                    </div>
                  </div>
                </div>
                <div class="col-xl-6 grid-margin stretch-card"/>
              </div>
            </div>
            <footer class="footer d-flex flex-column flex-md-row align-items-center justify-content-between">
              <p class="text-muted text-center text-md-left">
                Use is subject to full acceptance of Terms of Use and Privacy
                Policy, bar none. Investing is risky and can result in huge
                losses.
                <br /> Information provided is not financial advice and we do
                not warrant or guarantee the contents are accurate, up-to-date
                or reliable.{" "}
              </p>
              <p class="text-muted text-center text-md-left mb-0 d-none d-md-block">
                StockApe ©{" "}
                <a href="https://twitter.com/stock_ape">
                  <img src="twitter.svg" style={{ height: "20px" }} />
                </a>{" "}
                <a href="https://www.reddit.com/r/StockApeAI/">
                  <img
                    src="reddit1.svg"
                    id="redditbg"
                    style={{ height: "20px" }}
                  />
                </a>{" "}
              </p>
            </footer>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;
