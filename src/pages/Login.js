import React from "react";
import {Link} from "react-router-dom";

const Login = () => {
    return(
        <div>
            <div className="main-wrapper">
                <div className="page-wrapper full-page">
                    <div className="page-content d-flex align-items-center justify-content-center">

                        <div className="row w-100 mx-0 auth-page">
                            <div className="col-md-8 col-xl-6 mx-auto">
                                <div className="card">
                                    <div className="row">
                                        <div className="col-md-4 pr-md-0">
                                            <div className="auth-left-wrapper">

                                            </div>
                                        </div>
                                        <div className="col-md-8 pl-md-0">
                                            <div className="auth-form-wrapper px-4 py-5">

                                                <h5 className="text-muted font-weight-normal mb-4">Welcome back! Log in
                                                    to your account.</h5>
                                                <form className="forms-sample">
                                                    <div className="form-group">
                                                        <label htmlFor="exampleInputEmail1">Email address</label>
                                                        <input type="email" className="form-control"
                                                               id="exampleInputEmail1" placeholder="Email"/>
                                                    </div>
                                                    <div className="form-group">
                                                        <label htmlFor="exampleInputPassword1">Password</label>
                                                        <input type="password" className="form-control"
                                                               id="exampleInputPassword1"
                                                               autoComplete="current-password" placeholder="Password"/>
                                                    </div>
                                                    {/*<div className="form-check form-check-flat form-check-primary">*/}
                                                    {/*    <label className="form-check-label">*/}
                                                    {/*        <input type="checkbox" className="form-check-input"/>*/}
                                                    {/*            Remember me*/}
                                                    {/*    </label>*/}
                                                    {/*</div>*/}
                                                    <div className="mt-3">
                                                        <button type="button"
                                                                className="btn btn-outline-primary btn-icon-text mb-2 mb-md-0">
                                                            <Link to='/admin' >Login</Link>

                                                        </button>
                                                    </div>

                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
